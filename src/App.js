import React, { Component } from "react";

import Home from "./Home";
import About from "./About";
import Contact from "./Contact";
import gambar from "./gambar";
class App extends Component {
  render() {
    return (
      <div>
        <Home/>
        <About/>
        <Contact/>
        <Gambar/>
      </div>
    );
  }
}
export default App;